# WRF Intermediate files from netCDF-like Datasets

Jupiter Intelligence -- 30 June 2020
***

The *WRFInputFormatter* object in the `wps_formatter.py` file contains a number of methods that facilitate the processing of atmospheric and soil data in gridded netCDF-like datasets into the WRF intermediate format required for WRF preprocessing.  This code can replace the `ungrib.exe` portion of the WRF preprocessing sequence in many cases.  The formatting was derived from instructions in the WRF v3.8 User Guide (https://www2.mmm.ucar.edu/wrf/users/docs/user_guide_V3.8/users_guide_chap3.htm#_Writing_Meteorological_Data).

An example Jupyter notebook is provided (`example_usage.ipynb`) that illustrates how this process works from small example subsets of the NOAA/CIRES 20th Century Reanalysis v2 data that are provided in the `testdata/` folder.

Though the illustration here uses netCDF files on disk, there are other options for dataset formats that this object could be inherited or adjusted to process.  Because this code leverages the xarray (https://github.com/pydata/xarray) library for loading and manipulating netCDF-like data, the methods used can be very flexible.  Some possible extensions of this object's application could include (but not limited to):

*  Processing existing GRIB datasets instead of using `ungrib.exe` by leveraging the cfgrib capabilities now build in to xarray
*  Directly reading from THREDDS-hosted gridded datasets and processing into files suitable for WRF boundary conditions
*  Reading from Zarr-formatted datasets locally or in cloud-based object storage

## Support
Jupiter Intelligence has provided this code under the attached license.  Jupiter does not offer any ongoing support of this code; users can use and modify this code at their own risk.  While merge/pull requests, community contributions and suggested updates are welcome, they will only be reviewed by the Jupiter administration team as time allows for the forseeable future.

## Referenced libraries and data
```
Compo, G.P., J.S. Whitaker, P.D. Sardeshmukh, N. Matsui, R.J. Allan, X. Yin, B.E. Gleason, R.S. Vose, G. Rutledge, P. Bessemoulin, S. Brönnimann, M. Brunet, R.I. Crouthamel, A.N. Grant, P.Y. Groisman, P.D. Jones, M. Kruk, A.C. Kruger, G.J. Marshall, M. Maugeri, H.Y. Mok, Ø. Nordli, T.F. Ross, R.M. Trigo, X.L. Wang, S.D. Woodruff, and S.J. Worley, 2011: The Twentieth Century Reanalysis Project. Quarterly J. Roy. Meteorol. Soc., 137, 1-28.
```
```
Hoyer, S. & Hamman, J., (2017). xarray: N-D labeled Arrays and Datasets in Python. Journal of Open Research Software. 5(1), p.10. DOI: http://doi.org/10.5334/jors.148
```

